package org.yidu.novel.controller.base;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 设定文件配置基类
 * </p>
 * Copyright(c) 2014-2017 YiDu-Novel. All rights reserved.
 * 
 * @version 2.0.0
 * @author shinpa.you
 */
@Data
@EqualsAndHashCode(callSuper = false)
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "system")
public abstract class AbstractSettingBaseController extends AbstractServiceBaseController {

    /**
     * 网站标题
     */
    @Value("${system.title}")
    protected String title;
    /**
     * 网站关键字
     */
    @Value("${system.keywords}")
    protected String keywords;
    /**
     * 网站描述
     */
    @Value("${system.description}")
    protected String description;

    /**
     * 网站名称
     */
    @Value("${system.name}")
    protected String name;

    /**
     * 网站URL
     */
    @Value("${system.url}")
    protected String url;

    /**
     * 网站域名
     */
    @Value("${system.domain}")
    protected String domain;

    /**
     * 网站copyright
     */
    @Value("${system.copyright}")
    protected String copyright;

    /**
     * 网站支持
     */
    @Value("${system.support}")
    protected String support;

    /**
     * 网站beianNo
     */
    @Value("${system.beianNo}")
    protected String beianNo;

    /**
     * 网站统计代码
     */
    @Value("${system.analyticscode}")
    protected String analyticscode;

    /**
     * 启用缓存
     */
    @Value("${system.enableCache}")
    protected boolean enableCache;

    /**
     * 网站URI
     */
    @Value("${system.uri}")
    protected String uri;
    /**
     * 手机网站URI
     */
    @Value("${system.mobileUri}")
    protected String mobileUri;
    /**
     * 章节文件路径
     */
    @Value("${system.txtFilePath}")
    protected String txtFilePath;
    /**
     * 图片文件
     */
    @Value("${system.imagePath}")
    protected String imagePath;
    /**
     * 图片文件
     */
    @Value("${system.enableCleanUrl}")
    protected boolean enableCleanUrl;
    /**
     * 是否启用广告
     */
    @Value("${system.enableAd}")
    protected boolean enableAd;
    /**
     * 是否开启Gzip
     */
    @Value("${system.enableGzip}")
    protected boolean enableGzip;
    /**
     * 每页显示小说数
     */
    @Value("${system.countPerPage}")
    protected int countPerPage;
    /**
     * 最大收藏书
     */
    @Value("${system.maxBookcase}")
    protected int maxBookcase;
    /**
     * 小说介绍页表示评论件数
     */
    @Value("${system.reviewnum}")
    protected int reviewnum;
    /**
     * 小说介绍页表示评论件数
     */
    @Value("${system.numberPerPage}")
    protected int numberPerPage;
    /**
     * 生成静态首页
     */
    @Value("${system.enableCreateIndexPage}")
    protected boolean enableCreateIndexPage;
    /**
     * 生成静态首页
     */
    @Value("${system.enableCreateSiteMap}")
    protected boolean enableCreateSiteMap;
    /**
     * siteMap类型, 支持： xml、html 默认html
     */
    @Value("${system.enableCreateSiteMap}")
    protected String siteMapType;
    /**
     * 是否启用独立章节列表页面
     */
    @Value("${system.enableChapterIndexPage}")
    protected boolean enableChapterIndexPage;
    /**
     * 分类信息
     */
    protected Map<Integer, String> categoryMap;

    /**
     * 简介页显示的最新章节数
     */
    @Value("${system.infoLastChapterNum}")
    protected int infoLastChapterNum;
    
    /**
     * 是否开启伪原创
     */
    @Value("${system.enablePseudo}")
    protected boolean enablePseudo;
    
    

}
