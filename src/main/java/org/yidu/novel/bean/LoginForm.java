package org.yidu.novel.bean;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginForm implements Serializable {

    private static final long serialVersionUID = -5135858344252266812L;

    /**
     * loginid
     */
    @NotNull
    @Size(min = 6, max = 12)
    @Pattern(regexp = "[a-zA-Z0-9]*", message = "只支持英文和数字。")
    private String loginId;
    /**
     * 密码
     */
    @NotNull
    @Size(min = 6, max = 20)
    private String password;
    /**
     * cookie
     */
    private String useCookie;

}
