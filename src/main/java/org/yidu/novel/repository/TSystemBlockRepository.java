package org.yidu.novel.repository;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.yidu.novel.constant.YiDuConstants;
import org.yidu.novel.entity.TSystemBlock;

/**
 * <p>
 * 区块表操作类
 * </p>
 * Copyright(c) 2014-2017 YiDu-Novel. All rights reserved.
 * 
 * @version 2.0.0
 * @author shinpa.you
 */
public interface TSystemBlockRepository extends CrudRepository<TSystemBlock, Long> {

    @Query("FROM TSystemBlock WHERE target in (?1, " + YiDuConstants.BlockTarget.ALL_SITE + " )")
    @Cacheable(value = "yiduCache", key = "{ 'org.yidu.novel.repository.TSystemBlockRepository.findByTarget(short)',  #root.args[0] }")
    List<TSystemBlock> findByTargetContainsOrderBySystemBlockNoAsc(short type);

}
