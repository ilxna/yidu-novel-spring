package org.yidu.novel.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.yidu.novel.entity.TChapter;
import org.yidu.novel.repository.TChapterRepository;

@Service
public class ChapterService extends AbstractBaseService {

    @Autowired
    private TChapterRepository chapterRepository;

    /**
     * 通过小说编号去取小说信息
     * 
     * @param articleNo
     * 
     * @return 小说信息
     */
    @Cacheable(value = "yiduCache", key = "{'org.yidu.novel.service.ChapterService.findChapterByChapterNo(Long)', #chapterNo }")
    public TChapter findChapterByChapterNo(Long chapterNo) {
        return chapterRepository.findOne(chapterNo);
    }
}