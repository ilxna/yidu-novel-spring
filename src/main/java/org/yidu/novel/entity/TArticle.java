package org.yidu.novel.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.yidu.novel.constant.YiDuConstants;
import org.yidu.novel.controller.ArticleListController;
import org.yidu.novel.controller.AuthorArticleListController;
import org.yidu.novel.controller.BookcaseController;
import org.yidu.novel.controller.ChapterListController;
import org.yidu.novel.controller.DownloadController;
import org.yidu.novel.controller.InfoController;
import org.yidu.novel.controller.ReaderController;
import org.yidu.novel.controller.ReviewController;
import org.yidu.novel.controller.VoteController;
import org.yidu.novel.utils.Utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_article", indexes = { @Index(name = "t_article_allvisit_index", columnList = "allvisit", unique = false),
        @Index(name = "t_article_monthvisit_index", columnList = "monthvisit", unique = false),
        @Index(name = "t_article_weekvisit_index", columnList = "weekvisit", unique = false),
        @Index(name = "t_article_dayvisit_index", columnList = "dayvisit", unique = false),
        @Index(name = "t_article_allvote_index", columnList = "allvote", unique = false),
        @Index(name = "t_article_monthvote_index", columnList = "monthvote", unique = false),
        @Index(name = "t_article_weekvote_index", columnList = "weekvote", unique = false),
        @Index(name = "t_article_dayvote_index", columnList = "dayvote", unique = false),
        @Index(name = "t_article_size_index", columnList = "size", unique = false),
        @Index(name = "t_article_author_index", columnList = "author", unique = false),
        @Index(name = "t_article_pinyin_index", columnList = "pinyin", unique = false),
        @Index(name = "t_article_category_idx", columnList = "category", unique = false),
        @Index(name = "t_article_lastupdate_index", columnList = "lastupdate", unique = false),
        @Index(name = "t_article_postdate_index", columnList = "postdate", unique = false),
        @Index(name = "t_article_category_lastupdate_lastchapterno_deleteflag_idx", columnList = "category, lastupdate, lastchapterno, deleteflag", unique = false),
        @Index(name = "unique_index_articlename_author", columnList = "articlename,author", unique = true),
        @Index(name = "t_article_articlename_index", columnList = "articlename", unique = true) }, uniqueConstraints = @UniqueConstraint(columnNames = {
                "articlename", "author" }))
public class TArticle implements Serializable {

    private static final long serialVersionUID = -4352219013819495282L;

    @Id
    // @GeneratedValue(strategy = GenerationType.IDENTITY, generator =
    // "t_article_article_no_seq")
    // @GeneratedValue(strategy = GenerationType.AUTO)
    @SequenceGenerator(name = "t_article_seq", sequenceName = "t_article_articleno_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "t_article_seq")
    /**
     * 小说编号
     */
    @Column(name = "articleno")
    private Long articleNo;

    // fields
    /**
     * 小说名
     */
    @Column(name = "articlename")
    private String articleName;
    private String pinyin;
    @Column(name = "pinyinheadchar")
    private String pinyinHeadChar;
    private Character initial;
    private String keywords;
    @Column(name = "authorid")
    private Integer authorId;
    private String author;
    private Integer category;
    @Column(name = "subcategory")
    private Integer subCategory;
    private String intro;
    @Column(name = "lastchapterno")
    private Integer lastChapterNo;
    @Column(name = "lastchapter")
    private String lastChapter;
    @Column(name = "chapternum")
    private Integer chapterNum;
    private Integer size;
    @Column(name = "fullflag")
    private Boolean fullFlag;
    @Column(name = "imgflag")
    private Short imgFlag;
    private String agent;
    @Column(name = "firstflag")
    private Boolean firstFlag;
    private Integer permission;
    @Column(name = "authorflag")
    private Boolean authorFlag;
    @Column(name = "postdate")
    private Date postDate;
    @Column(name = "lastupdate")
    private Date lastUpdate;
    @Column(name = "dayvisit")
    private Integer dayVisit;
    @Column(name = "weekvisit")
    private Integer weekVisit;
    @Column(name = "monthvisit")
    private Integer monthVisit;
    @Column(name = "allvisit")
    private Integer allVisit;
    @Column(name = "dayvote")
    private Integer dayVote;
    @Column(name = "weekvote")
    private Integer weekVote;
    @Column(name = "monthvote")
    private Integer monthVote;
    @Column(name = "allvote")
    private Integer allVote;
    @Column(name = "deleteflag")
    private Boolean deleteFlag;
    @Column(name = "publicflag")
    private Integer publicFlag;
    @Column(name = "usecustomizeinfotitle")
    private Boolean useCustomizeInfoTitle;
    @Column(name = "infotitle")
    private String infoTitle;
    @Column(name = "infokeywords")
    private String infoKeywords;
    @Column(name = "infofescription")
    private String infoDescription;
    @Column(name = "usecustomizelisttitle")
    private Boolean useCustomizeListTitle;
    @Column(name = "listtitle")
    private String listTitle;
    @Column(name = "listkeywords")
    private String listKeywords;
    @Column(name = "listdescription")
    private String listDescription;
    @Column(name = "createuserno")
    private Long createUserNo;
    @Column(name = "createtime")
    private Date createTime;
    @Column(name = "modifyuserno")
    private Long modifyUserNo;
    @Column(name = "modifytime")
    private Date modifyTime;

    /**
     * 获取HTML格式的小说简介信息
     * 
     * @return HTML格式的小说简介信息
     */
    public String getIntroForHtml() {
        if (getIntro() != null) {
            // 替换换行和空格
            String html = StringEscapeUtils.escapeHtml4(intro);
            return html.replaceAll("\r\n", "<br/>").replaceAll("\n", "<br/>").replaceAll("\\s", "&nbsp;");
        }
        return null;
    }

    /**
     * 获得小说简介的缩略
     * 
     * @return 小说简介的缩略
     */
    public String getIntroOmit() {
        if (intro != null && intro.length() > 60) {
            return intro.substring(0, 60) + "...";
        }
        return intro;
    }

    public String getIntroOmit55() {
        if (intro != null && intro.length() > 55) {
            return intro.substring(0, 55) + "...";
        }
        return intro;
    }

    public String getIntroOmit30() {
        if (intro != null && intro.length() > 30) {
            return intro.substring(0, 30) + "...";
        }
        return intro;
    }

    /**
     * 获得子目录 <br>
     * 默认是小说号/1000
     * 
     * @return 子目录
     */
    public Long getSubdir() {
        return articleNo / YiDuConstants.SUB_DIR_ARTICLES;
    }

    /**
     * 获得最后更新时间
     * 
     * @return 最后更新时间
     */
    public String getLastupdateMin() {
        SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm:ss");
        return sdf1.format(lastUpdate);
    }

    /**
     * 获得图片URL
     * 
     * @return 图片URL
     */
    public String getImgUrl() {
        String fileName = "";
        if (getImgFlag() == null) {
            fileName = "nocover.jpg";
        } else {
            switch (getImgFlag()) {
            case 1:
                fileName = articleNo + "s.jpg";
                break;
            case 2:
                fileName = articleNo + "s.gif";
                break;
            case 3:
                fileName = articleNo + "s.png";
                break;
            case 10:
                fileName = articleNo + "l.jpg";
                break;
            default:
                fileName = "nocover.jpg";
                break;
            }
        }
        String imgUrl = YiDuConstants.imagePath + "/";
        if (StringUtils.equals("nocover.jpg", fileName)) {
            imgUrl = imgUrl + fileName;
        } else {
            if (YiDuConstants.enableCleanUrl) {
                imgUrl = imgUrl + getSubdir() + "-" + articleNo + "-" + Utils.convert2MD5(imgUrl + fileName) + fileName;
            } else {
                imgUrl = imgUrl + getSubdir() + "/" + articleNo + "/" + fileName;
            }
        }
        return imgUrl;
    }

    /**
     * 获取小说介绍页URL
     * 
     * @return 小说介绍页URL
     */
    public String getUrl() {
        String url = InfoController.URL + "?subdir=" + getSubdir() + "&articleno=" + articleNo;

        if (YiDuConstants.enablePinyinUrl) {
            url = InfoController.URL + "?pinyin=" + pinyin;
        }
        return url;
    }

    /**
     * 获取作者列表URL
     * 
     * @return 作者列表URL
     */
    public String getAuthorUrl() {
        return AuthorArticleListController.URL + "?author=" + author;
    }

    /**
     * 获取分类列表URL
     * 
     * @return 分类列表URL
     */
    public String getCategoryListUrl() {
        return ArticleListController.URL + "?category=" + category;
    }

    /**
     * 获取最新章节URL
     * 
     * @return 最新章节URL
     */
    public String getLastChapterUrl() {
        String url = ReaderController.URL + "?subdir=" + getSubdir() + "&articleno=" + articleNo + "&chapterno=" + lastChapterNo;

        if (YiDuConstants.enablePinyinUrl) {
            url = ReaderController.URL + "?pinyin=" + pinyin + "&chapterno=" + lastChapterNo;
        }
        return url;
    }

    /**
     * 获取最新章节URL
     * 
     * @return 最新章节URL
     */
    public String getThumbnailLastChapterUrl() {
        return ReaderController.URL + "?chapterno=" + lastChapterNo;
    }

    /**
     * 获取加入书签URL
     * 
     * @return 加入书签URL
     */
    public String getAddBookcaseUrl() {
        return BookcaseController.URL + "?articleNo=" + articleNo;
    }

    /**
     * 获取推荐小说URL
     * 
     * @return 推荐小说URL
     */
    public String getAddVoteUrl() {
        return VoteController.URL + "?articleNo=" + articleNo;
    }

    /**
     * 获取下载小说URL
     * 
     * @return 下载小说URL
     */
    public String getDownloadUrl() {
        return DownloadController.URL + "?subdir=" + getSubdir() + "&articleno=" + articleNo;
    }

    /**
     * 获取小说评价URL
     * 
     * @return 小说评价URL
     */
    public String getReviewUrl() {
        return ReviewController.URL + "?subdir=" + getSubdir() + "&articleno=" + articleNo;
    }

    /**
     * 获取章节列表URL
     * 
     * @return 分类列表URL
     */
    public String getChapterListUrl() {
        if (!YiDuConstants.enableChapterIndexPage) {
            return getUrl();
        } else {
            String url = ChapterListController.URL + "?subdir=" + getSubdir() + "&articleno=" + articleNo;
            if (YiDuConstants.enablePinyinUrl) {
                url = ChapterListController.URL + "?pinyin=" + pinyin;
            }
            return url;
        }
    }

    /**
     * 获取订阅URL
     * 
     * @return 订阅URL
     */
    // public String getSubscribeUrl() {
    // return response.encodeURL(SubscribeAction.URL + "!add?articleno=" +
    // getArticleno());
    // }

    // /**
    // * 获取小说介绍页拼音形式的URL
    // *
    // * @return 小说介绍页拼音形式的URL
    // */
    // public String getSingleBookUrl() {
    // String pinyin = SingleBookManager.getPinYinHeadChar(getArticleno());
    // if (StringUtils.isNotBlank(pinyin)) {
    // return "http://" + pinyin + "." +
    // YiDuConstants.yiduConf.getString(YiDuConfig.ROOT_DOMAIN);
    // } else {
    // return YiDuConstants.yiduConf.getString(YiDuConfig.ROOT_DOMAIN);
    // }
    // }

    /**
     * 获取小说tag的URL
     * 
     * @return 小说介绍页拼音形式的URL
     */
    // public List<TagDTO> getTagList() {
    // List<String> tags = Utils.getKeyWords(getArticlename());
    // List<TagDTO> tagList = new ArrayList<TagDTO>();
    // HttpServletResponse response = ServletActionContext.getResponse();
    // for (String tag : tags) {
    // TagDTO tagdto = new TagDTO();
    // tagdto.setTag(tag);
    // tagdto.setUrl(response.encodeURL(ArticleListAction.URL + "?tag=" + tag));
    // tagList.add(tagdto);
    // }
    // return tagList;
    // }

}
