package org.yidu.novel.controller;

import java.util.Locale;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.yidu.novel.constant.YiDuConstants;
import org.yidu.novel.controller.base.AbstractPublicBaseController;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 首页控制器
 * </p>
 * Copyright(c) 2014-2017 YiDu-Novel. All rights reserved.
 * 
 * @version 2.0.0
 * @author shinpa.you
 */
@Controller
@Data
@EqualsAndHashCode(callSuper = false)
public class IndexController extends AbstractPublicBaseController {

    /**
     * URL
     */
    public final static String URL = "/";

    /**
     * 主页
     */
    @RequestMapping(value = URL, method = RequestMethod.GET)
    public String index(Map<String, Object> model) {
        logger.debug(msg.getMessage("info.process.start", new String[] { "index" }, Locale.getDefault()));
        setCommonInfo2Model(model);
        logger.debug(msg.getMessage("info.process.end.normal", new String[] { "index" }, Locale.getDefault()));
        return getThemeName("index");
    }

    @Override
    protected Map<String, Object> getBlocks() {
        return loadBlocksByTarget(YiDuConstants.BlockTarget.INDEX);
    }

    @Override
    protected int getCurrentPageType() {
        return YiDuConstants.Pagetype.PAGE_INDEX;
    }

}