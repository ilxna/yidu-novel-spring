package org.yidu.novel.controller.base;

import java.util.Map;

/**
 * <p>
 * 用户画面基类
 * </p>
 * Copyright(c) 2014-2017 YiDu-Novel. All rights reserved.
 * 
 * @version 2.0.0
 * @author shinpa.you
 */
public abstract class AbstractAdminBaseController extends AbstractPublicAndUserBaseController {

    @Override
    protected Map<String, Object> getBlocks() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected int getCurrentPageType() {
        // TODO Auto-generated method stub
        return 0;
    }
}
