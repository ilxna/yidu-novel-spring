package org.yidu.novel.bean;

import java.io.Serializable;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ArticleSearchBean implements Serializable {
    private static final long serialVersionUID = -3074454586657224961L;

    private String author;
    private Integer category;
    private Boolean fullFlag;

}
