package org.yidu.novel.controller;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.yidu.novel.constant.YiDuConstants;
import org.yidu.novel.controller.base.AbstractPublicBaseController;
import org.yidu.novel.entity.TArticle;
import org.yidu.novel.entity.TChapter;
import org.yidu.novel.exception.NotFoundException;
import org.yidu.novel.utils.Utils;

/**
 * <p>
 * 阅读页控制器
 * </p>
 * Copyright(c) 2014-2017 YiDu-Novel. All rights reserved.
 * 
 * @version 2.0.0
 * @author shinpa.you
 */
@Controller
@Data
@EqualsAndHashCode(callSuper = false)
public class ReaderController extends AbstractPublicBaseController {
    /**
     * URL
     */
    public final static String URL = "/reader";

    /**
     * 小说简介页
     */
    @Transactional
    @RequestMapping(value = URL, method = RequestMethod.GET)
    public String reader(@RequestParam(value = "subdir", required = false) Long subdir, @RequestParam(value = "articleNo", required = false) Long articleNo,
            @RequestParam(value = "chapterNo", required = true) Integer chapterNo, @RequestParam(value = "pinyin", required = false) String pinyin,
            Map<String, Object> model) {

        logger.debug(msg.getMessage("info.process.start", new String[] { "reader" }, Locale.getDefault()));

        // 共通情報を設定する。
        setCommonInfo2Model(model);

        // 根据章节号取出章节信息
        TChapter chapter = chapterService.findChapterByChapterNo((long) chapterNo);

        if (null == chapter) {
            logger.warn("parameter was not specified.");
            // 存在しない場合、404を返す
            throw new NotFoundException();
        }

        // 用小说编号来获取小说信息
        // 这里不用缓存，因为要更新统计信息
        TArticle article = articleService.findArticleByArticleNo(chapter.getArticleNo());
        model.put("article", article);
        chapter.setPinyin(article.getPinyin());

        // 获取下一章信息
        Pageable pageable = new PageRequest(0, 1);
        List<TChapter> nextChapterList = chapterRepository.findByArticleNoAndChapterNoGreaterThanOrderByChapterNoAsc(articleNo, (long) chapterNo, pageable);
        if (CollectionUtils.isNotEmpty(nextChapterList)) {
            chapter.setNextChapterNo(nextChapterList.get(0).getChapterNo().intValue());
        }
        // 获取上一章信息
        List<TChapter> preChapterList = chapterRepository.findByArticleNoAndChapterNoLessThanOrderByChapterNoDesc(articleNo, (long) chapterNo, pageable);
        if (CollectionUtils.isNotEmpty(preChapterList)) {
            chapter.setPreChapterNo(preChapterList.get(0).getChapterNo().intValue());
        }

        // 获得章节正文
        chapter.setContent(Utils.getContext(chapter, true, enablePseudo));
        model.put("chapter", chapter);

        // 更新统计信息
        articleRepository.updateStatisticsInfo(chapter.getArticleNo());

        logger.debug(msg.getMessage("info.process.end.normal", new String[] { "reader" }, Locale.getDefault()));
        return getThemeName("reader");
    }

    @Override
    protected Map<String, Object> getBlocks() {
        return loadBlocksByTarget(YiDuConstants.BlockTarget.READER_PAGE);
    }

    @Override
    protected int getCurrentPageType() {
        return YiDuConstants.Pagetype.PAGE_READER;
    }
}