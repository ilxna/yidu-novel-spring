package org.yidu.novel.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.yidu.novel.entity.TArticle;
import org.yidu.novel.repository.TArticleRepository;

@Service
public class ArticleService extends AbstractBaseService {

    @Autowired
    private TArticleRepository articleRepository;

    /**
     * 通过小说编号去取小说信息
     * 
     * @param articleNo
     * 
     * @return 小说信息
     */
    @Cacheable(value = "yiduCache", key = "{'org.yidu.novel.service.ArticleService.findArticleByNo(Long)', #articleNo }")
    public TArticle findArticleByArticleNo(Long articleNo) {
        return articleRepository.findOne(articleNo);
    }
}