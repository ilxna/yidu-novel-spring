package org.yidu.novel.controller;

import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.yidu.novel.constant.YiDuConstants;
import org.yidu.novel.repository.TArticleRepository;

@Controller
@Data
@EqualsAndHashCode(callSuper = false)
public class SearchController {

    /**
     * 当前画面类型
     */
    private int pageType = YiDuConstants.Pagetype.PAGE_INDEX;

    @Autowired
    private TArticleRepository articleRepository;
    /**
     * URL
     */
    public final static String URL = "/search";

    /**
     * 主页
     */
    @Transactional
    @RequestMapping(value = URL, method = RequestMethod.GET)
    public String info(@RequestParam(value = "subdir", required = false) String subdir,
            @RequestParam(value = "articleNo", required = false) String articleNo,
            @RequestParam(value = "pinyin", required = false) String pinyin, Map<String, Object> model) {

        return "top";
    }
}