package org.yidu.novel.repository;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.yidu.novel.entity.TChapter;

/**
 * <p>
 * 章节表操作类
 * </p>
 * Copyright(c) 2014-2017 YiDu-Novel. All rights reserved.
 * 
 * @version 2.0.0
 * @author shinpa.you
 */
public interface TChapterRepository extends PagingAndSortingRepository<TChapter, Long> {

    /**
     * 按小说编号获取章节列表
     * 
     * @param pageable
     * @return 章节列表
     */
    @Cacheable(value = "yiduCache", key = "{ 'org.yidu.novel.repository.TChapterRepository.findByArticleNoOrderByChapterNoAsc(Long)', #root.args[0] }")
    List<TChapter> findByArticleNoOrderByChapterNoAsc(Long articleNo);

    /**
     * 按小说编号获取前几章
     * 
     * @param articleNo
     * @param pageable
     * @return 章节列表
     */
    @Cacheable(value = "yiduCache", key = "{ 'org.yidu.novel.repository.TChapterRepository.findByArticleNoOrderByChapterNoDesc(Long, Pageable)', #root.args[0], #root.args[1] }")
    List<TChapter> findByArticleNoOrderByChapterNoDesc(Long articleNo, Pageable pageable);

    /**
     * 按小说编号和章节编号获取上一章
     * 
     * @param articleNo
     * @param chapterNo
     * @param pageable
     * @return 章节列表
     */
    @Cacheable(value = "yiduCache", key = "{ 'org.yidu.novel.repository.TChapterRepository.findByArticleNoAndChapterLessThanOrderByChapterNoDesc(Long, Long, Pageable)', #root.args[0], #root.args[1], #root.args[2] }")
    List<TChapter> findByArticleNoAndChapterNoLessThanOrderByChapterNoDesc(Long articleNo, Long chapterNo,
            Pageable pageable);

    /**
     * 按小说编号和章节编号获取下一章
     * 
     * @param articleNo
     * @param chapterNo
     * @param pageable
     * @return 章节列表
     */
    @Cacheable(value = "yiduCache", key = "{ 'org.yidu.novel.repository.TChapterRepository.findByArticleNoAndChapterNoGreaterThanOrderByChapterNoAsc(Long, Long, Pageable)', #root.args[0], #root.args[1], #root.args[2] }")
    List<TChapter> findByArticleNoAndChapterNoGreaterThanOrderByChapterNoAsc(Long articleNo, Long chapterNo,
            Pageable pageable);
}
