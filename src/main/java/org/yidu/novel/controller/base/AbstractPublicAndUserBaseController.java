package org.yidu.novel.controller.base;

import java.util.Locale;
import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 公开画面和用户画面的共同基类
 * </p>
 * Copyright(c) 2014-2017 YiDu-Novel. All rights reserved.
 * 
 * @version 2.0.0
 * @author shinpa.you
 */
@Data
@EqualsAndHashCode(callSuper = false)
public abstract class AbstractPublicAndUserBaseController extends AbstractSettingBaseController {

    /**
     * 获得当前页面使用的区块信息
     * 
     * @return 区块信息
     */
    protected abstract Map<String, Object> getBlocks();

    /**
     * 取得当前页的类型
     * 
     * <pre>
     * 1:主页
     * 2：小说列表
     * 3：小说介绍页
     * 4：小说阅读页
     * 11：登录页
     * 99：其他页
     * </pre>
     * 
     * @return 当前页的类型
     */
    protected abstract int getCurrentPageType();

    /**
     * 获取小说作者
     * 
     * @return 小说作者
     */
    protected String getAuthor() {
        return null;
    };

    /**
     * 获取相关小说名
     * 
     * @return 小说名
     */
    protected String getRelativeArticleName() {
        return null;
    };

    /**
     * 设置共同的返回值
     * 
     * @param model
     */
    protected void setCommonInfo2Model(Map<String, Object> model) {
        model.put("title", title);
        model.put("keywords", keywords);
        model.put("description", description);
        model.put("name", name);
        model.put("url", url);
        model.put("domain", domain);
        model.put("copyright", copyright);
        model.put("support", support);
        model.put("beianNo", beianNo);
        model.put("analyticscode", analyticscode);
        model.put("categoryMap", categoryMap);
        model.put("blocks", getBlocks());
        model.put("pageType", getCurrentPageType());
    }

    /**
     * 根据页面类型取得相应的区块信息
     * 
     * <pre>
     * 1:主页
     * 2：小说列表
     * 3：小说介绍页
     * 4：小说阅读页
     * 11：登录页
     * 99：其他页
     * </pre>
     * 
     * @param page
     * 
     * @return 区块信息
     */
    protected Map<String, Object> loadBlocksByTarget(short target) {
        logger.debug(msg.getMessage("info.process.start", new String[] { "loadBlock" }, Locale.getDefault()));
        // 区块信息
        return blockService.loadBlocks(target, getAuthor());
    }

    protected String getThemeName(String pageName) {
        return pageName;
    }
}
