package org.yidu.novel.repository;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.yidu.novel.entity.TUser;

/**
 * <p>
 * 用户表操作类
 * </p>
 * Copyright(c) 2014-2017 YiDu-Novel. All rights reserved.
 * 
 * @version 2.0.0
 * @author shinpa.you
 */
public interface TUserRepository extends CrudRepository<TUser, Long> {
    /**
     * 根据登录id查找用户信息
     * 
     * @return 用户信息
     */
    @Query("FROM TUser WHERE loginId = ?1 ")
    @Cacheable(value = "yiduCache", key = "{ 'org.yidu.novel.repository.TUserRepository.findByLoginId(String)', #root.args[0] }")
    List<TUser> findByLoginId(String loginId);

    /**
     * 根据登录id查找用户数
     * 
     * @return 用户数
     */
    @Cacheable(value = "yiduCache", key = "{ 'org.yidu.novel.repository.TUserRepository.countByLoginId(String)', #root.args[0] }")
    Integer countByLoginId(String loginId);

}
