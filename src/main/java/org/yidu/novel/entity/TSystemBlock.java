package org.yidu.novel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_system_block")
public class TSystemBlock implements Serializable {

    private static final long serialVersionUID = -3679355196597237251L;

    @Id
    @SequenceGenerator(name = "t_system_block_seq", sequenceName = "t_system_block_blockno_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "t_system_block_seq")
    // primary key
    @Column(name = "blockno")
    private Integer blockNo;

    // fields
    @Column(name = "blockid")
    private String blockId;
    @Column(name = "blockname")
    private String blockName;
    private Short type;
    private Integer category;
    @Column(name = "sortcol")
    private String sortCol;
    @Column(name = "isasc")
    private Boolean isAsc;
    @Column(name = "isfinish")
    private Boolean isFinish;
    @Column(name = "limitnum")
    private Integer limitNum;
    private String content;
    private Short target;
    @Column(name = "deleteflag")
    private Boolean deleteFlag;
    @Column(name = "modifyuserno")
    private Integer modifyUserNo;
    @Column(name = "modifytime")
    private Date modifyTime;
}
