package org.yidu.novel.controller;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.yidu.novel.constant.YiDuConstants;
import org.yidu.novel.controller.base.AbstractPublicBaseController;
import org.yidu.novel.entity.TArticle;
import org.yidu.novel.entity.TChapter;
import org.yidu.novel.entity.TReview;
import org.yidu.novel.exception.NotFoundException;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 首页控制器
 * </p>
 * Copyright(c) 2014-2017 YiDu-Novel. All rights reserved.
 * 
 * @version 2.0.0
 * @author shinpa.you
 */
@Controller
@Data
@EqualsAndHashCode(callSuper = false)
public class InfoController extends AbstractPublicBaseController {

    /**
     * URL
     */
    public final static String URL = "/info";

    /**
     * 小说简介页
     */
    @RequestMapping(value = URL, method = RequestMethod.GET)
    public String info(@RequestParam(value = "subdir", required = false) Long subdir, @RequestParam(value = "articleNo", required = false) Long articleNo,
            @RequestParam(value = "pinyin", required = false) String pinyin, Map<String, Object> model) {

        logger.debug(msg.getMessage("info.process.start", new String[] { "info" }, Locale.getDefault()));

        // 共通情報を設定する。
        setCommonInfo2Model(model);
        // 用小说编号或pinyin来获取小说信息
        TArticle article = null;
        if (articleNo != null) {
            article = articleService.findArticleByArticleNo(articleNo);
        } else if (StringUtils.isNotBlank(pinyin)) {
            List<TArticle> articleList = articleRepository.findByPinyin(pinyin);
            if (CollectionUtils.isNotEmpty(articleList)) {
                article = articleList.get(0);
            }
        }

        if (null == article) {
            logger.warn("parameter was not specified.");
            // 存在しない場合、404を返す
            throw new NotFoundException();
        }

        model.put("article", article);

        // 获取最后更新的几章
        Pageable pageable = new PageRequest(0, infoLastChapterNum);
        List<TChapter> chapterList = chapterRepository.findByArticleNoOrderByChapterNoDesc(article.getArticleNo(), pageable);
        model.put("chapterList", chapterList);

        if (!enableChapterIndexPage) {
            // 如果没有启动独立章节页的话获得章节信息
            List<TChapter> allChapterList = chapterRepository.findByArticleNoOrderByChapterNoAsc(article.getArticleNo());
            model.put("allChapterList", allChapterList);
        }

        // 获取评论信息
        List<TReview> reviewList = reviewRepository.findByArticleNoOrderByReviewNoDesc(article.getArticleNo(), pageable);
        model.put("reviewList", reviewList);

        logger.debug(msg.getMessage("info.process.end.normal", new String[] { "info" }, Locale.getDefault()));
        return getThemeName("info");
    }

    @Override
    protected Map<String, Object> getBlocks() {
        return loadBlocksByTarget(YiDuConstants.BlockTarget.ARTICLE_INFO);
    }

    @Override
    protected int getCurrentPageType() {
        return YiDuConstants.Pagetype.PAGE_ARTICLE_INFO;
    }
}