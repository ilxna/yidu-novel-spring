package org.yidu.novel.controller;

import java.util.Locale;
import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.yidu.novel.controller.base.AbstractPublicBaseController;

/**
 * <p>
 * 书架
 * </p>
 * Copyright(c) 2014-2017 YiDu-Novel. All rights reserved.
 * 
 * @version 2.0.0
 * @author shinpa.you
 */
@Controller
@Data
@EqualsAndHashCode(callSuper = false)
public class BookcaseController extends AbstractPublicBaseController {

    /**
     * URL
     */
    public final static String URL = "/bookcase";

    /**
     * 添加数据
     */
    @Transactional
    @RequestMapping(value = URL, method = RequestMethod.GET)
    public String bookcase(@RequestParam(value = "articleNo", required = true) Long articleNo, Map<String, Object> model) {
        logger.debug(msg.getMessage("info.process.start", new String[] { "bookcase" }, Locale.getDefault()));
        setCommonInfo2Model(model);
        model.put("message", "add bookcase successfully.");
        logger.debug(msg.getMessage("info.process.end.normal", new String[] { "bookcase" }, Locale.getDefault()));
        return getThemeName("success");
    }

    @Override
    protected Map<String, Object> getBlocks() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected int getCurrentPageType() {
        // TODO Auto-generated method stub
        return 0;
    }

}
