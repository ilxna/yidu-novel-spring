package org.yidu.novel.bean;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.springframework.validation.annotation.Validated;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Validated
public class RegeditForm implements Serializable {

    private static final long serialVersionUID = -5961755194857194588L;
    /**
     * loginid
     */
    @NotNull
    @Size(min = 6, max = 12)
    private String loginId;
    /**
     * 密码
     */
    @NotNull
    @Size(min = 6, max = 20)
    private String password;
    /**
     * 密码
     */
    @NotNull
    @Size(min = 6, max = 20)
    private String repassword;
    /**
     * email
     */
    @NotNull
    @Email
    private String email;

    /**
     * sex
     */
    private Integer sex;

    /**
     * qq
     */
    private String qq;
    /**
     * mobileNo
     */
    private String mobileNo;
}
