package org.yidu.novel.controller;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.yidu.novel.constant.YiDuConstants;
import org.yidu.novel.controller.base.AbstractPublicBaseController;
import org.yidu.novel.entity.TArticle;
import org.yidu.novel.entity.TChapter;
import org.yidu.novel.exception.NotFoundException;

/**
 * <p>
 * 小说列表页控制器
 * </p>
 * Copyright(c) 2014-2017 YiDu-Novel. All rights reserved.
 * 
 * @version 2.0.0
 * @author shinpa.you
 */
@Controller
@Data
@EqualsAndHashCode(callSuper = false)
public class ChapterListController extends AbstractPublicBaseController {

    /**
     * URL
     */
    public final static String URL = "/chapterList";

    /**
     * 主页
     */
    @Transactional
    @RequestMapping(value = URL, method = RequestMethod.GET)
    public String info(@RequestParam(value = "subdir", required = false) Long subdir,
            @RequestParam(value = "articleNo", required = false) Long articleNo,
            @RequestParam(value = "pinyin", required = false) String pinyin, Map<String, Object> model) {

        logger.debug(msg.getMessage("info.process.start", new String[] { "chapterList" }, Locale.getDefault()));

        // 共通情報を設定する。
        setCommonInfo2Model(model);
        // 用小说编号或pinyin来获取小说信息
        TArticle article = null;
        if (articleNo != null) {
            article = articleService.findArticleByArticleNo(articleNo);
        } else if (StringUtils.isNotBlank(pinyin)) {
            List<TArticle> articleList = articleRepository.findByPinyin(pinyin);
            if (CollectionUtils.isNotEmpty(articleList)) {
                article = articleList.get(0);
            }
        }

        if (null == article) {
            logger.warn("parameter was not specified.");
            // 存在しない場合、404を返す
            throw new NotFoundException();
        }

        model.put("article", article);

        // 如果没有启动独立章节页的话获得章节信息
        List<TChapter> allChapterList = chapterRepository.findByArticleNoOrderByChapterNoAsc(article.getArticleNo());
        model.put("allChapterList", allChapterList);

        logger.debug(msg.getMessage("info.process.end.normal", new String[] { "chapterList" }, Locale.getDefault()));
        return getThemeName("chapterList");
    }

    @Override
    protected Map<String, Object> getBlocks() {
        return loadBlocksByTarget(YiDuConstants.BlockTarget.CHAPTER_LIST);
    }

    @Override
    protected int getCurrentPageType() {
        return YiDuConstants.Pagetype.PAGE_CHAPTER_LIST;
    }
}