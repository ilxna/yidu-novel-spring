package org.yidu.novel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Index;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_user", indexes = { @Index(name = "t_user_loginid_password_deleteflag_index", columnList = "loginid,password,deleteflag", unique = false),
        @Index(name = "t_user_openid_deleteflag_index", columnList = "openid,deleteflag", unique = false) })
public class TUser implements Serializable {

    private static final long serialVersionUID = -685853876083205195L;

    @Id
    @SequenceGenerator(name = "t_user_seq", sequenceName = "t_user_userno_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "t_user_seq")
    @Column(name = "userno")
    private Integer userNo;

    // fields
    @Column(name = "loginid")
    private String loginId;
    private String password;
    @Column(name = "username")
    private String userName;
    private String email;
    @Column(name = "regdate")
    private Date regDate;
    private Short sex;
    private String qq;
    @Column(name = "lastlogin")
    private Date lastLogin;
    @Column(name = "lineno")
    private String lineNo;
    private Short type;
    @Column(name = "votecount")
    private Integer voteCount;
    @Column(name = "deleteflag")
    private Boolean deleteFlag;
    @Column(name = "realname")
    private String realName;
    private String id;
    @Column(name = "mobileno")
    private String mobileNo;
    private String branch;
    @Column(name = "bankno")
    private String bankNo;
    @Column(name = "alipayacount")
    private String alipayAcount;
    private Integer category;
    @Column(name = "subcategory")
    private Integer subCategory;
    @Column(name = "modifyuserno")
    private Long modifyUserNo;
    @Column(name = "modifytime")
    private Date modifyTime;
    @Column(name = "openid")
    private String openId;
    @Column(name = "activedflag")
    private Boolean activedFlag;
    @Column(name = "mailtoken")
    private String mailToken;
}
