package org.yidu.novel.controller.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

/**
 * <p>
 * 画面基类
 * </p>
 * Copyright(c) 2014-2017 YiDu-Novel. All rights reserved.
 * 
 * @version 2.0.0
 * @author shinpa.you
 */
public abstract class AbstractBaseController {

    /**
     * 输出log
     */
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected MessageSource msg;
}
