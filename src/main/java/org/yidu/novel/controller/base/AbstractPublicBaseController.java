package org.yidu.novel.controller.base;

/**
 * <p>
 * 公开画面基类
 * </p>
 * Copyright(c) 2014-2017 YiDu-Novel. All rights reserved.
 * 
 * @version 2.0.0
 * @author shinpa.you
 */
public abstract class AbstractPublicBaseController extends AbstractPublicAndUserBaseController {

}
