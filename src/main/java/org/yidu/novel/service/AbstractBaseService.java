package org.yidu.novel.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

public abstract class AbstractBaseService {

    /**
     * 输出log
     */
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected MessageSource msg;
}
