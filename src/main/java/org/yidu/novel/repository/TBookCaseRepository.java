package org.yidu.novel.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.yidu.novel.entity.TBookCase;

/**
 * <p>
 * 书架表操作类
 * </p>
 * Copyright(c) 2014-2017 YiDu-Novel. All rights reserved.
 * 
 * @version 2.0.0
 * @author shinpa.you
 */
public interface TBookCaseRepository extends PagingAndSortingRepository<TBookCase, Long> {

}
