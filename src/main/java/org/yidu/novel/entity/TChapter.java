package org.yidu.novel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.yidu.novel.constant.YiDuConstants;
import org.yidu.novel.controller.ChapterListController;
import org.yidu.novel.controller.InfoController;
import org.yidu.novel.controller.ReaderController;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_chapter", indexes = { @Index(name = "t_chapter_articleno_chapterno_index", columnList = "articleno,chapterno", unique = false),
        @Index(name = "t_chapter_chaptername_index", columnList = "chaptername", unique = false),
        @Index(name = "t_chapter_articlename_chaptername_index", columnList = "articlename,chaptername", unique = true) })
public class TChapter implements Serializable {

    private static final long serialVersionUID = 5157867030225141142L;

    @Id
    // primary key
    @SequenceGenerator(name = "t_chapter_seq", sequenceName = "t_chapter_chapterno_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "t_chapter_seq")
    @Column(name = "chapterno")
    private Long chapterNo;

    // fields
    @Column(name = "articleno")
    private Long articleNo;
    @Column(name = "articlename")
    private String articleName;
    @Column(name = "chaptertype")
    private Short chapterType;
    @Column(name = "chaptername")
    private String chapterName;
    private Integer size;
    @Column(name = "isvip")
    private Boolean isVip;
    @Column(name = "postdate")
    private Date postDate;
    @Column(name = "deleteflag")
    private Boolean deleteFlag;
    @Column(name = "publishtime")
    private Date publishTime;
    @Column(name = "modifyuserno")
    private Integer modifyUserNo;
    @Column(name = "modifytime")
    private Date modifyTime;

    /**
     * 下一章章节编号
     */
    @Transient
    private int nextChapterNo;
    /**
     * 上一章章节编号
     */
    @Transient
    private int preChapterNo;
    /**
     * 章节内容
     */
    @Transient
    private String content;

    /**
     * 拼音
     */
    @Transient
    private String pinyin;

    /**
     * 获得子目录 <br>
     * 默认是小说号/1000
     * 
     * @return 子目录
     */
    public Long getSubdir() {
        return articleNo / YiDuConstants.SUB_DIR_ARTICLES;
    }

    /**
     * 获取章节列表URL
     * 
     * @return 分类列表URL
     */
    public String getUrl() {
        String url = ReaderController.URL + "?subdir=" + getSubdir() + "&articleNo=" + articleNo + "&chapterNo=" + chapterNo;
        if (YiDuConstants.enablePinyinUrl) {
            url = ReaderController.URL + "?pinyin=" + pinyin + "&chapterNo=" + chapterNo;
        }
        return url;
    }

    /**
     * 获取小说介绍页URL
     * 
     * @return 小说介绍页URL
     */
    public String getInfoUrl() {
        return InfoController.URL + "?subdir=" + getSubdir() + "&articleNo=" + articleNo;
    }

    // /**
    // * 获取 content
    // *
    // * @return content
    // */
    // public String getContent() {
    // String keywords =
    // YiDuConstants.yiduConf.getString(YiDuConfig.FILTER_KEYWORD);
    // String[] keywordArr = StringUtils.split(keywords, "#");
    // for (String string : keywordArr) {
    // content = content.replaceAll(string, "");
    // }
    // return content;
    // }
    //
    // public String getEsccapeContent() {
    // String escapeContent = StringUtils.replace(this.getContent(), "<",
    // "&lt;");
    // escapeContent = StringUtils.replace(escapeContent, ">", "&gt;");
    // return escapeContent;
    // }
    //
    // public String getReplacedContent() {
    // String keywords = YiDuConstants.yiduConf.getString("filterKeyWord");
    // String[] keywordArr = StringUtils.split(keywords, ",");
    // String replaced = getContent();
    // for (String string : keywordArr) {
    // replaced = replaced.replaceAll(string, "");
    // }
    // return replaced;
    // }
    //
    // /**
    // *
    // * 设置content
    // *
    // *
    // * @param content
    // * content
    // */
    // public void setContent(String content) {
    // this.content = (content == null ? "" : content);
    // }

    /**
     * 获取章节列表URL
     *
     * @return 分类列表URL
     */
    public String getChapterListUrl() {

        if (YiDuConstants.enableChapterIndexPage) {
            String url = ChapterListController.URL + "?subdir=" + getSubdir() + "&articleNo=" + articleNo;
            if (YiDuConstants.enablePinyinUrl) {
                url = ChapterListController.URL + "?pinyin=" + pinyin;
            }
            return url;
        } else {
            return getInfoUrl();
        }

    }

    /**
     * 获取下一章章节URL
     *
     * @return 下一章章节URL
     */

    public String getNextChapterUrl() {
        if (nextChapterNo != 0) {
            String url = ReaderController.URL + "?subdir=" + getSubdir() + "&articleNo=" + articleNo + "&chapterNo=" + nextChapterNo;

            if (YiDuConstants.enablePinyinUrl) {
                url = ReaderController.URL + "?pinyin=" + pinyin + "&chapterNo=" + nextChapterNo;
            }
            return url;
        } else {
            if (YiDuConstants.enableChapterIndexPage) {
                return getChapterListUrl();
            } else {
                String url = getInfoUrl();
                if (YiDuConstants.enablePinyinUrl) {
                    if (YiDuConstants.enableChapterIndexPage) {
                        url = ChapterListController.URL + "?pinyin=" + pinyin;
                    } else {
                        url = InfoController.URL + "?pinyin=" + pinyin;
                    }
                }
                return url;
            }
        }
    }

    /**
     * 获取下一章章节URL
     *
     * @return 上一章章节URL
     */
    public String getPreChapterUrl() {
        if (preChapterNo != 0) {
            String url = ReaderController.URL + "?subdir=" + getSubdir() + "&articleNo=" + articleNo + "&chapterNo=" + preChapterNo;

            if (YiDuConstants.enablePinyinUrl) {
                url = ReaderController.URL + "?pinyin=" + pinyin + "&chapterNo=" + preChapterNo;
            }
            return url;

        } else {
            if (YiDuConstants.enableChapterIndexPage) {
                return getChapterListUrl();
            } else {
                String url = getInfoUrl();
                if (YiDuConstants.enableChapterIndexPage) {
                    url = ChapterListController.URL + "?pinyin=" + pinyin;
                } else {
                    url = InfoController.URL + "?pinyin=" + pinyin;
                }
                return url;
            }
        }
    }

    // /**
    // * 获取下一章章节URL
    // *
    // * @return 下一章章节URL
    // */
    //
    // public String getNextChapterThumbnailUrl() {
    // HttpServletResponse response = ServletActionContext.getResponse();
    // return response.encodeURL(ReaderAction.URL + "?chapterno=" +
    // getNextChapterno());
    // }
    //
    // /**
    // * 获取下一章章节URL
    // *
    // * @return 上一章章节URL
    // */
    // public String getPreChapterThumbnailUrl() {
    // HttpServletResponse response = ServletActionContext.getResponse();
    // return response.encodeURL(ReaderAction.URL + "?chapterno=" +
    // getPreChapterno());
    // }

}
