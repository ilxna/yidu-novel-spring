package org.yidu.novel.bean;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RuleForm implements Serializable {

    private static final long serialVersionUID = 7757863893344465286L;
    /**
     * 规则版本
     */
    @NotNull
    private String ruleVersion;
    /**
     * 站点名称
     */
    @NotNull
    private String getSiteName;
    /**
     * 站点编码
     */
    @NotNull
    private String getSiteCharset;
    /**
     * 站点地址
     */
    @NotNull
    private String getSiteUrl;
    /**
     * 搜索页地址
     */
    private String novelSearchUrl;
    /**
     * 测试用搜索小说名
     */
    private String testSearchNovelName;
    /**
     * 目标站小说号
     */
    private String novelSearchGetNovelKey;
    /**
     * 目标站小说名
     */
    private String novelSearchGetNovelName;
    /**
     * 列表地址
     */
    private String novelListUrl;
    /**
     * 小说编号
     */
    private String novelListGetNovelKey;
    /**
     * 小说编号忽略大小写
     */
    // @Pattern(regexp = "IgnoreCase")
    private String novelListGetNovelKeyIgnoreCase;
    /**
     * 真实小说编号
     */
    private String novelListGetNovelKey2;
    /**
     * 真实小说编号忽略大小写
     */
    // @Pattern(regexp = "IgnoreCase")
    private String novelListGetNovelKey2IgnoreCase;
    /**
     * 小说信息页地址
     */
    private String novelUrl;
    /**
     * 小说名
     */
    private String novelName;
    /**
     * 小说名忽略大小写
     */
    // @Pattern(regexp = "IgnoreCase")
    private String novelNameIgnoreCase;
    /**
     * 小说名替换
     */
    private String novelNameFilterPattern;
    /**
     * 获得小说作者
     */
    private String novelAuthor;
    /**
     * 小说作者忽略大小写
     */
    // @Pattern(regexp = "IgnoreCase")
    private String novelAuthorIgnoreCase;
    /**
     * 小说大类
     */
    private String lagerSort;
    /**
     * 小说大类忽略大小写
     */
    // @Pattern(regexp = "IgnoreCase")
    private String lagerSortIgnoreCase;
    /**
     * 小说小类
     */
    private String smallSort;
    /**
     * 小说小类忽略大小写
     */
    // @Pattern(regexp = "IgnoreCase")
    private String smallSortIgnoreCase;
    /**
     * 小说简介
     */
    private String novelIntro;
    /**
     * 小说简介忽略大小写
     */
    // @Pattern(regexp = "IgnoreCase")
    private String novelIntroIgnoreCase;
    /**
     * 小说简介替换
     */
    private String novelIntroFilterPattern;
    /**
     * 关键字
     */
    private String novelKeyword;
    /**
     * 关键字忽略大小写
     */
    // @Pattern(regexp = "IgnoreCase")
    private String novelKeywordIgnoreCase;
    /**
     * 写作进度
     */
    private String novelDegree;
    /**
     * 写作进度忽略大小写
     */
    // @Pattern(regexp = "IgnoreCase")
    private String novelDegreeIgnoreCase;
    /**
     * 写作进度替换
     */
    private String novelDegreeFilterPattern;
    /**
     * 小说封面
     */
    private String novelCover;
    /**
     * 小说封面忽略大小写
     */
    // @Pattern(regexp = "IgnoreCase")
    private String novelCoverIgnoreCase;
    /**
     * 小说默认封面
     */
    private String novelDefaultCoverUrl;
    /**
     * 信息页额外信息
     */
    private String novelInfoExtra;
    /**
     * 信息页额外信息忽略大小写
     */
    // @Pattern(regexp = "IgnoreCase")
    private String novelInfoExtraIgnoreCase;
    /**
     * 信息页额外信息替换
     */
    private String novelInfoExtraFilterPattern;
    /**
     * 目录页地址
     */
    private String novelInfoGetNovelPubKey;
    /**
     * 目录页地址忽略大小写
     */
    // @Pattern(regexp = "IgnoreCase")
    private String novelInfoGetNovelPubKeyIgnoreCase;
    /**
     * 目录页地址
     */
    private String pubIndexUrl;
    /**
     * 章节范围
     */
    private String pubChapterRegion;
    /**
     * 章节信息替换
     */
    private String pubChapterRegionFilterPattern;
    /**
     * 章节范围忽略大小写
     */
    // @Pattern(regexp = "IgnoreCase")
    private String pubChapterRegionIgnoreCase;
    /**
     * 章节名
     */
    private String pubChapterName;
    /**
     * 章节名忽略大小写
     */
    // @Pattern(regexp = "IgnoreCase")
    private String pubChapterNameIgnoreCase;
    /**
     * 章节名替换
     */
    private String pubChapterNameFilterPattern;
    /**
     * 章节地址
     */
    private String pubChapterGetChapterKey;
    /**
     * 章节地址忽略大小写
     */
    // @Pattern(regexp = "IgnoreCase")
    private String pubChapterGetChapterKeyIgnoreCase;
    /**
     * 内容页地址
     */
    private String pubContentUrl;
    /**
     * 真实内容页地址
     */
    private String pubContentUrl2;
    /**
     * 章节内容
     */
    private String pubContentText;
    /**
     * 章节内容忽略大小写
     */
    // @Pattern(regexp = "IgnoreCase")
    private String pubContentTextIgnoreCase;
    /**
     * 章节内容从ASCII转为NATIVE
     */
    // @Pattern(regexp = "True")
    private String pubContentTextASCII;
    /**
     * 全角转半角（PubContentText_BJ2QJ）
     */
    // @Pattern(regexp = "True")
    private String pubContentTextBJ2QJ;
    /**
     * 繁体转简体（PubContentText_FT2JT）
     */
    // @Pattern(regexp = "True")
    private String pubContentTextFT2JT;
    /**
     * 章节内容替换
     */
    private String pubContentTextFilterPattern;
    /**
     * 规则文件名
     */
    private String ruleFileName;

}
