package org.yidu.novel.controller.admin;

import java.util.Locale;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.yidu.novel.controller.base.AbstractAdminBaseController;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 管理后台首页控制器
 * </p>
 * Copyright(c) 2014-2018 YiDu-Novel. All rights reserved.
 * 
 * @version 2.0.0
 * @author shinpa.you
 */
@Controller
@Data
@EqualsAndHashCode(callSuper = false)
public class AdminIndexController extends AbstractAdminBaseController {

    /**
     * URL
     */
    public final static String URL = "/admin/index";

    /**
     * 主页
     */
    @RequestMapping(value = URL, method = RequestMethod.GET)
    public String index(Map<String, Object> model) {
        logger.debug(msg.getMessage("info.process.start", new String[] { "index" }, Locale.getDefault()));

        logger.debug(msg.getMessage("info.process.end.normal", new String[] { "index" }, Locale.getDefault()));
        return "admin/index";
    }

}