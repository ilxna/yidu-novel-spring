package org.yidu.novel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_review", indexes = { @Index(name = "t_review_articleno_index", columnList = "articleno", unique = false) })
public class TReview implements Serializable {

    private static final long serialVersionUID = -6789254394292546036L;

    @Id
    // primary key
    @SequenceGenerator(name = "t_review_seq", sequenceName = "t_review_reviewno_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "t_review_seq")
    @Column(name = "reviewno")
    private Integer reviewNo;

    // fields
    @Column(name = "userno")
    private Long userNo;
    @Column(name = "loginid")
    private String loginId;
    @Column(name = "articleno")
    private Long articleNo;
    @Column(name = "articlename")
    private String articleName;
    @Column(name = "chapterno")
    private Long chapterNo;
    @Column(name = "chaptername")
    private String chapterName;
    private String title;
    private String review;
    private String email;
    @Column(name = "postdate")
    private Date postDate;
    @Column(name = "deleteflag")
    private Boolean deleteFlag;
    @Column(name = "modifyuserno")
    private Integer modifyUserNo;
    @Column(name = "modifytime")
    private Date modifyTime;
}
