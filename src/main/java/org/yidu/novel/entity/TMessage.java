package org.yidu.novel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_message")
public class TMessage implements Serializable {
    private static final long serialVersionUID = 5974455397709649040L;

    @Id
    // primary key
    @SequenceGenerator(name = "t_message_seq", sequenceName = "t_message_messageno_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "t_message_seq")
    @Column(name = "messageno")
    private Integer messageNo;

    // fields
    @Column(name = "userno")
    private Long userNo;
    @Column(name = "loginid")
    private String loginId;
    @Column(name = "touserno")
    private Long toUserNo;
    @Column(name = "tologinid")
    private String toLoginId;
    private String title;
    private String content;
    private Short category;
    @Column(name = "isread")
    private Boolean isRead;
    @Column(name = "postdate")
    private Date postDate;
    @Column(name = "deleteflag")
    private Boolean deleteFlag;
    @Column(name = "modifyuserno")
    private Integer modifyUserNo;
    @Column(name = "modifytime")
    private Date modifyTime;
}
