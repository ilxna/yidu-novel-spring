function showDialog() {
	$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
		_title: function(title) {
			var $title = this.options.title || '&nbsp;';
			if( ("title_html" in this.options) && this.options.title_html == true )
				title.html($title);
			else title.text($title);
		}
	}));
	$("#console").removeClass('hide').dialog({
		modal: true,
		title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa fa-check'></i> 测试信息</h4></div>",
		title_html: true,
		width:900,
		position: ['center', 'top'],
		close:function(){
			Console.clear();
		},
		buttons: [ 
			{
				text: "关闭",
				"class" : "btn btn-primary btn-xs",
				click: function() {
					$( this ).dialog( "close" ); 
				}
			}
		]
	});
}
var Console = {};
Console.log = (function(message) {
    var console = document.getElementById('console');
    var p = document.createElement('p');
    p.style.wordWrap = 'break-word';
    p.innerHTML = message;
    console.appendChild(p);
});
Console.clear = (function() {
    var console = document.getElementById('console');
    console.innerHTML = "";
});
// 测试规则
function testRule() {
	var param = $("#ruleForm").serialize() + getParam();
	$.ajax({
		dataType: "text",
		url: "/test",
		data: param,
		type:"POST",
		async: false,
		success: function(_msg){
			showDialog();
			Console.log(_msg);
		}
	});
}
function getParam() {
	var p = "&";
	$("#ruleForm input[type=checkbox]").each(function(){
    	p = p + $(this).attr("name").substring(1) + "=" + $(this).is(':checked') + "&";
    });
	return p;
}
function escapeSymbol(s) {
	return s.replace(/\&/g,'%26');
}