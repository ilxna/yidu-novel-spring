package org.yidu.novel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_bookcase")
public class TBookCase implements Serializable {
    private static final long serialVersionUID = 558109724938307501L;

    @Id
    // primary key
    @Column(name = "bookcaseno")
    @SequenceGenerator(name = "t_bookcase_seq", sequenceName = "t_bookcase_bookcaseno_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "t_bookcase_seq")
    private Integer bookcaseNo;

    // fields
    @Column(name = "articleno")
    private Long articleNo;
    @Column(name = "articlename")
    private String articleName;
    private Integer category;
    @Column(name = "userno")
    private Long userNo;
    @Column(name = "username")
    private String userName;
    @Column(name = "chapterno")
    private Long chapterNo;
    @Column(name = "chaptername")
    private String chapterName;
    @Column(name = "lastvisit")
    private Date lastVisit;
    @Column(name = "createtime")
    private Date createTime;
    @Column(name = "deleteflag")
    private Boolean deleteFlag;
    @Column(name = "modifyuserno")
    private Integer modifyUserNo;
    @Column(name = "modifytime")
    private Date modifyTime;
}
