package org.yidu.novel.controller;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.yidu.novel.constant.YiDuConstants;
import org.yidu.novel.controller.base.AbstractPublicBaseController;
import org.yidu.novel.entity.TArticle;
import org.yidu.novel.repository.TArticleRepository;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 小说列表页控制器
 * </p>
 * Copyright(c) 2014-2017 YiDu-Novel. All rights reserved.
 * 
 * @version 2.0.0
 * @author shinpa.you
 */
@Controller
@Data
@EqualsAndHashCode(callSuper = false)
public class QQLoginController extends AbstractPublicBaseController {
    /**
     * 当前画面类型
     */
    private int pageType = YiDuConstants.Pagetype.PAGE_INDEX;

    @Autowired
    private TArticleRepository articleRepository;
    /**
     * URL
     */
    public final static String URL = "/qqLogin";

    /**
     * 主页
     */
    @Transactional
    @RequestMapping(value = URL, method = RequestMethod.GET)
    public String info(@RequestParam(value = "subdir", required = false) String subdir, @RequestParam(value = "articleNo", required = false) String articleNo,
            @RequestParam(value = "pinyin", required = false) String pinyin, Map<String, Object> model) {

        // 查询规则文件夹下的所有规则文件， 列出来
//        model.put("blocks", blockService.getBlocks());
        TArticle article = null;
        if (StringUtils.isNotBlank(articleNo)) {
            article = articleRepository.findOne(Long.valueOf(articleNo));
        } else if (StringUtils.isNotBlank(pinyin)) {
            List<TArticle> articleList = articleRepository.findByPinyin(pinyin);
            article = articleList.get(0);
        }

        if (null == article) {
            logger.warn("parameter was not specified.");
            // TODO error page
            return "error";
        }
        model.put("article", article);
        logger.debug("index process end.");
        return "info";
    }

    @Override
    protected Map<String, Object> getBlocks() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected int getCurrentPageType() {
        // TODO Auto-generated method stub
        return 0;
    }
}