package org.yidu.novel.controller.base;

/**
 * <p>
 * 用户画面基类
 * </p>
 * Copyright(c) 2014-2017 YiDu-Novel. All rights reserved.
 * 
 * @version 2.0.0
 * @author shinpa.you
 */
public abstract class AbstractUserBaseController extends AbstractPublicAndUserBaseController {

}
