package org.yidu.novel.controller;

import java.util.Locale;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.yidu.novel.constant.YiDuConstants;
import org.yidu.novel.controller.base.AbstractPublicBaseController;
import org.yidu.novel.entity.TArticle;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 作家小说列表页控制器
 * </p>
 * Copyright(c) 2014-2017 YiDu-Novel. All rights reserved.
 * 
 * @version 2.0.0
 * @author shinpa.you
 */
@Controller
@Data
@EqualsAndHashCode(callSuper = false)
public class AuthorArticleListController extends AbstractPublicBaseController {

    /**
     * URL
     */
    public final static String URL = "/authorArticleList";

    /**
     * 小说列表页
     */
    @RequestMapping(value = URL, method = RequestMethod.GET)
    public String authorArticleList(@RequestParam(value = "author", required = true) String author,
            @RequestParam(value = "page", required = false) Integer page, Map<String, Object> model) {
        logger.debug(msg.getMessage("info.process.start", new String[] { "authorArticleList" }, Locale.getDefault()));
        // 设置默认页码
        if (page == null || page < 1) {
            page = 1;
        }
        setCommonInfo2Model(model);
        model.put("author", author);
        model.put("page", page);

        // 該当分類を取得する。
        Pageable pageable = new PageRequest(page - 1, countPerPage);
        Page<TArticle> articleList = articleRepository.findByAuthorAndLastChapterIsNotNull(author, pageable);
        model.put("articleList", articleList);
        logger.debug(msg.getMessage("info.process.end.normal", new String[] { "authorArticleList" },
                Locale.getDefault()));
        return getThemeName("authorArticleList");
    }

    @Override
    protected Map<String, Object> getBlocks() {
        return loadBlocksByTarget(YiDuConstants.BlockTarget.ARTICLE_LIST);
    }

    @Override
    protected int getCurrentPageType() {
        return YiDuConstants.Pagetype.PAGE_ARTICLE_LIST;
    }

}