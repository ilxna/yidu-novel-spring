package org.yidu.novel.repository;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.yidu.novel.entity.TReview;

/**
 * <p>
 * 评论表操作类
 * </p>
 * Copyright(c) 2014-2017 YiDu-Novel. All rights reserved.
 * 
 * @version 2.0.0
 * @author shinpa.you
 */
public interface TReviewRepository extends PagingAndSortingRepository<TReview, Long> {
    /**
     * 取得最新的评论
     * 
     * @param articleNo
     * @param pageable
     * @return 评论列表
     */
    @Cacheable(value = "yiduCache", key = "{ 'org.yidu.novel.repository.TReviewRepository.findByArticleNoOrderByReviewNoDesc(Long, Pageable)', #root.args[0], #root.args[1] }")
    List<TReview> findByArticleNoOrderByReviewNoDesc(Long articleNo, Pageable pageable);
}
