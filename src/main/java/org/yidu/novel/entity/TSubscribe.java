package org.yidu.novel.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_subscribe")
public class TSubscribe implements Serializable {

    private static final long serialVersionUID = 4585281521439407232L;

    @Id
    @SequenceGenerator(name = "t_subscribe_seq", sequenceName = "t_subscribe_subscribeno_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "t_subscribe_seq")
    // primary key
    @Column(name = "subscribeno")
    private Integer subscribeNo;

    // fields
    @Column(name = "userno")
    private Long userNo;
    @Column(name = "articleno")
    private Long articleNo;
}
