package org.yidu.novel.controller.base;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.springframework.beans.factory.annotation.Autowired;
import org.yidu.novel.repository.TArticleRepository;
import org.yidu.novel.repository.TBookCaseRepository;
import org.yidu.novel.repository.TChapterRepository;
import org.yidu.novel.repository.TMessageRepository;
import org.yidu.novel.repository.TReviewRepository;
import org.yidu.novel.repository.TSystemBlockRepository;
import org.yidu.novel.repository.TUserRepository;
import org.yidu.novel.service.ArticleService;
import org.yidu.novel.service.BlockService;
import org.yidu.novel.service.ChapterService;

/**
 * <p>
 * 画面用service基类
 * </p>
 * Copyright(c) 2014-2017 YiDu-Novel. All rights reserved.
 * 
 * @version 2.0.0
 * @author shinpa.you
 */
@Data
@EqualsAndHashCode(callSuper = false)
public abstract class AbstractServiceBaseController extends AbstractBaseController {

    @Autowired
    protected BlockService blockService;
    @Autowired
    protected ArticleService articleService;
    @Autowired
    protected ChapterService chapterService;

    @Autowired
    protected TArticleRepository articleRepository;
    @Autowired
    protected TBookCaseRepository bookCaseRepository;
    @Autowired
    protected TChapterRepository chapterRepository;
    @Autowired
    protected TMessageRepository messageRepository;
    @Autowired
    protected TReviewRepository reviewRepository;
    @Autowired
    protected TSystemBlockRepository systemBlockRepository;
    @Autowired
    protected TUserRepository userRepository;

}
